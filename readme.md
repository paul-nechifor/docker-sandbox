# Docker Sandbox

A service for running arbitrary commands inside a Docker container.

## TODO

- Fix the fact that instances aren't started concurrently (possibly because the
  headers aren't sent).

## License

MIT
